# cso-csharp-nar-repack

Was a private project back then November 2019.

The goal is to repackaging your folder into a NAR archive file.
Compression mode is not supported yet.

All credits goes to `Da_FileServer` for his/her CSOTools.

Extra credit goes to `NekoMeow`/`Xein` for figuring out the filename origin string encoding.