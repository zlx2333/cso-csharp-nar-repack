﻿using System;
using System.IO;
using System.Text;

namespace Nexon.CSO.Extension
{
    public sealed class FileEncoder : Stream
    {
        private Stream BaseStream { get; set; }
        private byte[] Key { get; set; }

        private FileEncoder()
        {
            Key = new byte[16];
        }

        public FileEncoder(Stream baseStream, string path) : this()
        {
            BaseStream = baseStream;
            GenerateKey(path);
        }

        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return true; } }

        public override bool CanWrite { get { return false; } }

        public override long Length
        {
            get
            {
                return BaseStream.Length;
            }
        }

        public override long Position
        {
            get
            {
                return BaseStream.Position;
            }
            set
            {
                BaseStream.Position = value;
            }
        }

        private void GenerateKey(string path)
        {
            uint num = PythonHash(Encoding.GetEncoding("euc-kr").GetBytes(path));
            for (int index = 0; index < 16; ++index)
            {
                num = (uint)((int)num * 1103515245 + 12345);
                Key[index] = (byte)(num & (uint)byte.MaxValue);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (!disposing)
                return;
            BaseStream.Dispose();
        }

        public override void Flush()
        {
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            long position = Position;
            int num = BaseStream.Read(buffer, offset, count);
            for (int index = 0; index < num; ++index)
                buffer[offset + index] ^= Key[position + (long)index & 15L];
            return num;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return BaseStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException("Cannot write to stream.");
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException("Cannot write to stream.");
        }

        public static uint PythonHash(byte[] data)
        {
            uint num = 0;
            for (int index = 0; index < data.Length; ++index)
                num = num * 1000003U ^ (uint)data[index];
            return num ^ (uint)data.Length;
        }
    }
}
