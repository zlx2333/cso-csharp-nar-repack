﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.FilesListView
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  public class FilesListView : ListView
  {
    private IContainer components;
    private ColumnHeader colName;
    private ColumnHeader colLastModified;
    private ColumnHeader colSize;
    private ContextMenuStrip contextMenuStrip;
    private ToolStripMenuItem extractToolStripMenuItem;
    private ToolStripMenuItem verifyToolStripMenuItem;

    public event EventHandler<FilesEventArgs> ExtractFiles;

    public event EventHandler<FilesEventArgs> VerifyFiles;

    public FilesListView()
    {
      this.InitializeComponent();
      this.ListViewItemSorter = (IComparer) new FilesListView.ColumnSort(this)
      {
        Column = -1,
        Order = SortOrder.None
      };
    }

    public string FullPath { get; set; }

    public int SortColumn
    {
      get => this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter && listViewItemSorter.Column >= 0 && listViewItemSorter.Column < this.Columns.Count ? listViewItemSorter.Column : -1;
      set
      {
        if (!(this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter))
          return;
        listViewItemSorter.Column = value;
        this.Sort();
      }
    }

    public SortOrder SortColumnOrder
    {
      get => this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter && listViewItemSorter.Column >= 0 && listViewItemSorter.Column < this.Columns.Count ? listViewItemSorter.Order : SortOrder.None;
      set
      {
        if (!(this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter))
          return;
        listViewItemSorter.Order = value;
        this.Sort();
      }
    }

    protected void OnExtractFiles(FilesEventArgs e)
    {
      if (e == null)
        throw new ArgumentNullException(nameof (e));
      if (this.ExtractFiles == null)
        return;
      this.ExtractFiles((object) this, e);
    }

    protected void OnVerifyFiles(FilesEventArgs e)
    {
      if (e == null)
        throw new ArgumentNullException(nameof (e));
      if (this.VerifyFiles == null)
        return;
      this.VerifyFiles((object) this, e);
    }

    private void FilesListView_ColumnClick(object sender, ColumnClickEventArgs e)
    {
      if (this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter)
      {
        if (listViewItemSorter.Column != e.Column)
        {
          listViewItemSorter.Column = e.Column;
          listViewItemSorter.Order = SortOrder.Ascending;
        }
        else
        {
          switch (listViewItemSorter.Order)
          {
            case SortOrder.None:
            case SortOrder.Descending:
              listViewItemSorter.Order = SortOrder.Ascending;
              break;
            case SortOrder.Ascending:
              listViewItemSorter.Order = SortOrder.Descending;
              break;
          }
        }
        this.SetSortIcon(listViewItemSorter.Column, listViewItemSorter.Order);
        this.Sort();
      }
      else
        this.SetSortIcon(-1, SortOrder.None);
    }

    private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
    {
      if (this.SelectedIndices.Count > 0)
        return;
      e.Cancel = true;
    }

    private void extractToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.SelectedIndices.Count <= 0)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      foreach (ListViewItem selectedItem in this.SelectedItems)
      {
        if (selectedItem.Tag is NexonArchiveFileEntry tag)
          archiveFileEntryList.Add(tag);
      }
      this.OnExtractFiles(new FilesEventArgs(this.FullPath, (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    private void verifyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.SelectedIndices.Count <= 0)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      foreach (ListViewItem selectedItem in this.SelectedItems)
      {
        if (selectedItem.Tag is NexonArchiveFileEntry tag)
          archiveFileEntryList.Add(tag);
      }
      this.OnVerifyFiles(new FilesEventArgs(this.FullPath, (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    protected override void CreateHandle()
    {
      base.CreateHandle();
      if (!(this.ListViewItemSorter is FilesListView.ColumnSort listViewItemSorter))
        return;
      this.SetSortIcon(listViewItemSorter.Column, listViewItemSorter.Order);
    }

    private static string GetHumanSize(long size)
    {
      if (size < 0L)
        return string.Empty;
      double num1 = (double) size;
      if (num1 <= 1024.0)
        return num1.ToString("n0", (IFormatProvider) NumberFormatInfo.CurrentInfo) + (num1 == 1.0 ? " byte" : " bytes");
      double num2 = num1 / 1024.0;
      if (num2 <= 1024.0)
        return num2.ToString("n0", (IFormatProvider) NumberFormatInfo.CurrentInfo) + " KB";
      double num3 = num2 / 1024.0;
      return num3 > 1024.0 ? (num3 / 1024.0).ToString("n2", (IFormatProvider) NumberFormatInfo.CurrentInfo) + " GB" : num3.ToString("n2", (IFormatProvider) NumberFormatInfo.CurrentInfo) + " MB";
    }

    public ListViewItem AddFile(NexonArchiveFileEntry file)
    {
      ListViewItem listViewItem = file != null ? new ListViewItem(Path.GetFileName(file.Path)) : throw new ArgumentNullException(nameof (file));
      listViewItem.Tag = (object) file;
      listViewItem.SubItems.Add(file.LastModifiedTime.ToString((IFormatProvider) DateTimeFormatInfo.CurrentInfo));
      listViewItem.SubItems.Add(FilesListView.GetHumanSize(file.Size));
      this.Items.Add(listViewItem);
      return listViewItem;
    }

    private void SetSortIcon(int columnIndex, SortOrder order)
    {
      IntPtr hWnd = NativeMethods.SendMessage(this.Handle, 4127, IntPtr.Zero, IntPtr.Zero);
      for (int index = 0; index <= this.Columns.Count - 1; ++index)
      {
        IntPtr wParam = new IntPtr(index);
        NativeMethods.LVCOLUMN lPLVCOLUMN = new NativeMethods.LVCOLUMN();
        lPLVCOLUMN.mask = 4;
        NativeMethods.SendMessage(hWnd, 4619, wParam, ref lPLVCOLUMN);
        if (order != SortOrder.None && index == columnIndex)
        {
          switch (order)
          {
            case SortOrder.Ascending:
              lPLVCOLUMN.fmt &= -513;
              lPLVCOLUMN.fmt |= 1024;
              break;
            case SortOrder.Descending:
              lPLVCOLUMN.fmt &= -1025;
              lPLVCOLUMN.fmt |= 512;
              break;
          }
        }
        else
          lPLVCOLUMN.fmt &= -1537;
        NativeMethods.SendMessage(hWnd, 4620, wParam, ref lPLVCOLUMN);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.colName = new ColumnHeader();
      this.colLastModified = new ColumnHeader();
      this.colSize = new ColumnHeader();
      this.contextMenuStrip = new ContextMenuStrip(this.components);
      this.extractToolStripMenuItem = new ToolStripMenuItem();
      this.verifyToolStripMenuItem = new ToolStripMenuItem();
      this.contextMenuStrip.SuspendLayout();
      this.SuspendLayout();
      this.colName.Text = "Name";
      this.colName.Width = 150;
      this.colLastModified.Text = "Last Modified";
      this.colLastModified.Width = 150;
      this.colSize.Text = "Size";
      this.colSize.Width = 80;
      this.contextMenuStrip.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.extractToolStripMenuItem,
        (ToolStripItem) this.verifyToolStripMenuItem
      });
      this.contextMenuStrip.Name = "contextMenuStrip";
      this.contextMenuStrip.Size = new Size(119, 48);
      this.contextMenuStrip.Opening += new CancelEventHandler(this.contextMenuStrip_Opening);
      this.extractToolStripMenuItem.Name = "extractToolStripMenuItem";
      this.extractToolStripMenuItem.Size = new Size(118, 22);
      this.extractToolStripMenuItem.Text = "E&xtract...";
      this.extractToolStripMenuItem.Click += new EventHandler(this.extractToolStripMenuItem_Click);
      this.verifyToolStripMenuItem.Name = "verifyToolStripMenuItem";
      this.verifyToolStripMenuItem.Size = new Size(118, 22);
      this.verifyToolStripMenuItem.Text = "&Verify";
      this.verifyToolStripMenuItem.Click += new EventHandler(this.verifyToolStripMenuItem_Click);
      this.Columns.AddRange(new ColumnHeader[3]
      {
        this.colName,
        this.colLastModified,
        this.colSize
      });
      this.ContextMenuStrip = this.contextMenuStrip;
      this.FullRowSelect = true;
      this.HideSelection = false;
      this.View = View.Details;
      this.ColumnClick += new ColumnClickEventHandler(this.FilesListView_ColumnClick);
      this.contextMenuStrip.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    private sealed class ColumnSort : IComparer
    {
      private FilesListView listView;
      public int Column = -1;
      public SortOrder Order;

      public ColumnSort(FilesListView listView) => this.listView = listView;

      public int Compare(object x, object y)
      {
        ListViewItem listViewItem1 = x as ListViewItem;
        ListViewItem listViewItem2 = y as ListViewItem;
        if (listViewItem1 == null)
          return listViewItem2 == null ? 0 : -1;
        if (listViewItem2 == null)
          return 1;
        if (this.Column < 0 || this.Order == SortOrder.None || this.Order != SortOrder.Ascending && this.Order != SortOrder.Descending)
          return 0;
        NexonArchiveFileEntry tag1 = listViewItem1.Tag as NexonArchiveFileEntry;
        NexonArchiveFileEntry tag2 = listViewItem2.Tag as NexonArchiveFileEntry;
        if (this.Column >= listViewItem1.SubItems.Count && this.Column >= listViewItem2.SubItems.Count)
          return 0;
        int num;
        switch (this.Column)
        {
          case 1:
            if (tag1 != null && tag2 != null)
            {
              num = DateTime.Compare(tag1.LastModifiedTime, tag2.LastModifiedTime);
              break;
            }
            goto default;
          case 2:
            if (tag1 != null && tag2 != null)
            {
              num = tag1.Size.CompareTo(tag2.Size);
              break;
            }
            goto default;
          default:
            num = string.Compare(listViewItem1.SubItems[this.Column].Text, listViewItem2.SubItems[this.Column].Text, StringComparison.CurrentCultureIgnoreCase);
            break;
        }
        return this.Order == SortOrder.Descending ? -num : num;
      }
    }
  }
}
