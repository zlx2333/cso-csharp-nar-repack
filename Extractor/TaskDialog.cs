﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.TaskDialog
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  public class TaskDialog : Form
  {
    private Predicate<NexonArchiveFileEntry> task;
    private ICollection<NexonArchiveFileEntry> entries;
    private string destinationPath;
    private IContainer components;
    private PathLabel sourcePathLabel;
    private PathLabel destinationPathLabel;
    private ProgressBar progressBar;
    private Button cancelButton;
    private BackgroundWorker backgroundWorker;

    public TaskDialog(
      Predicate<NexonArchiveFileEntry> task,
      ICollection<NexonArchiveFileEntry> entries)
    {
      if (task == null)
        throw new ArgumentNullException(nameof (task));
      if (entries == null)
        throw new ArgumentNullException(nameof (entries));
      this.task = task;
      this.entries = entries;
      this.InitializeComponent();
    }

    public string DestinationPath
    {
      get => this.destinationPath;
      set => this.destinationPath = value;
    }

    private void NexusArchiveTaskDialog_Load(object sender, EventArgs e) => this.backgroundWorker.RunWorkerAsync();

    private void cancelButton_Click(object sender, EventArgs e)
    {
      if (this.backgroundWorker.IsBusy)
      {
        this.backgroundWorker.CancelAsync();
        this.progressBar.Style = ProgressBarStyle.Marquee;
      }
      else
      {
        this.DialogResult = DialogResult.Cancel;
        this.Close();
      }
    }

    private void InitializeProgressWork(object state) => this.progressBar.Style = ProgressBarStyle.Continuous;

    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      lock (this.entries)
      {
        TaskDialog.ProgressUpdate progressUpdate = new TaskDialog.ProgressUpdate();
        foreach (NexonArchiveFileEntry entry in (IEnumerable<NexonArchiveFileEntry>) this.entries)
          progressUpdate.TotalFileSize += entry.Size;
        if (this.backgroundWorker.CancellationPending)
        {
          e.Cancel = true;
        }
        else
        {
          this.Invoke((Delegate) new Action<object>(this.InitializeProgressWork), new object[1]);
          if (this.backgroundWorker.CancellationPending)
          {
            e.Cancel = true;
          }
          else
          {
            foreach (NexonArchiveFileEntry entry in (IEnumerable<NexonArchiveFileEntry>) this.entries)
            {
              this.backgroundWorker.ReportProgress(0, (object) entry.Path);
              if (!this.task(entry))
                throw new ApplicationException("Task sent an abort code.");
              if (this.backgroundWorker.CancellationPending)
              {
                e.Cancel = true;
                break;
              }
              progressUpdate.CurrentFileSize += entry.Size;
              this.backgroundWorker.ReportProgress(0, (object) progressUpdate);
            }
          }
        }
      }
    }

    private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      if (e.UserState is TaskDialog.ProgressUpdate userState)
      {
        int width = this.progressBar.ClientSize.Width;
        this.progressBar.Maximum = width;
        this.progressBar.Value = Convert.ToInt32(userState.CurrentFileSize * (long) width / userState.TotalFileSize);
      }
      else
      {
        string szUserState = e.UserState as string;
        if (string.IsNullOrEmpty(szUserState))
          return;
        string str = szUserState.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        int num = 0;
        while ((int) str[num] == (int) Path.DirectorySeparatorChar)
          ++num;
        string path2 = str.Substring(num);
        this.sourcePathLabel.Text = path2;
        if (this.destinationPath == null)
          return;
        this.destinationPathLabel.Text = Path.Combine(this.destinationPath, path2);
      }
    }

    private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if (e.Error != null)
        this.DialogResult = DialogResult.Abort;
      else if (e.Cancelled)
      {
        this.DialogResult = DialogResult.Cancel;
      }
      else
      {
        this.progressBar.Value = this.progressBar.Maximum;
        this.DialogResult = DialogResult.OK;
      }
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.progressBar = new ProgressBar();
      this.cancelButton = new Button();
      this.backgroundWorker = new BackgroundWorker();
      this.destinationPathLabel = new PathLabel();
      this.sourcePathLabel = new PathLabel();
      Label label1 = new Label();
      Label label2 = new Label();
      this.SuspendLayout();
      label1.AutoSize = true;
      label1.Location = new Point(12, 9);
      label1.Name = "label1";
      label1.Size = new Size(44, 13);
      label1.TabIndex = 0;
      label1.Text = "&Source:";
      label2.AutoSize = true;
      label2.Location = new Point(12, 29);
      label2.Name = "label2";
      label2.Size = new Size(63, 13);
      label2.TabIndex = 2;
      label2.Text = "&Destination:";
      this.progressBar.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.progressBar.Location = new Point(12, 52);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new Size(380, 23);
      this.progressBar.Style = ProgressBarStyle.Marquee;
      this.progressBar.TabIndex = 4;
      this.cancelButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.cancelButton.DialogResult = DialogResult.Cancel;
      this.cancelButton.FlatStyle = FlatStyle.System;
      this.cancelButton.Location = new Point(317, 81);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new Size(75, 23);
      this.cancelButton.TabIndex = 5;
      this.cancelButton.Text = "&Cancel";
      this.cancelButton.UseVisualStyleBackColor = true;
      this.cancelButton.Click += new EventHandler(this.cancelButton_Click);
      this.backgroundWorker.WorkerReportsProgress = true;
      this.backgroundWorker.WorkerSupportsCancellation = true;
      this.backgroundWorker.DoWork += new DoWorkEventHandler(this.backgroundWorker_DoWork);
      this.backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
      this.backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
      this.destinationPathLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.destinationPathLabel.Location = new Point(81, 29);
      this.destinationPathLabel.Name = "destinationPathLabel";
      this.destinationPathLabel.Size = new Size(311, 13);
      this.destinationPathLabel.TabIndex = 3;
      this.destinationPathLabel.Text = "n/a";
      this.sourcePathLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.sourcePathLabel.Location = new Point(81, 9);
      this.sourcePathLabel.Name = "sourcePathLabel";
      this.sourcePathLabel.Size = new Size(311, 13);
      this.sourcePathLabel.TabIndex = 1;
      this.sourcePathLabel.Text = "n/a";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.CancelButton = (IButtonControl) this.cancelButton;
      this.ClientSize = new Size(404, 116);
      this.Controls.Add((Control) this.cancelButton);
      this.Controls.Add((Control) this.progressBar);
      this.Controls.Add((Control) this.destinationPathLabel);
      this.Controls.Add((Control) label2);
      this.Controls.Add((Control) this.sourcePathLabel);
      this.Controls.Add((Control) label1);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "NexonArchiveTaskDialog";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Task Dialog...";
      this.Shown += new EventHandler(this.NexusArchiveTaskDialog_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private sealed class ProgressUpdate
    {
      public long CurrentFileSize;
      public long TotalFileSize;
    }
  }
}
