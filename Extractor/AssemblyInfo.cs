﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: Guid("f0bc4e3d-81d0-48ea-923e-0519b02a5297")]
[assembly: AssemblyDescription("A tool for viewing and extracting Nexon archive files. These files are used by Counter-Strike Online.")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("Nexon Archive Extractor")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("CSOTools")]
[assembly: AssemblyCopyright("Copyright © Da_FileServer 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.0.0.0")]
