﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.Properties.Settings
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default => Settings.defaultInstance;

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool AutoDecryptModels
    {
      get => (bool) this[nameof (AutoDecryptModels)];
      set => this[nameof (AutoDecryptModels)] = (object) value;
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("Normal")]
    [UserScopedSetting]
    public FormWindowState FormWindowState
    {
      get => (FormWindowState) this[nameof (FormWindowState)];
      set => this[nameof (FormWindowState)] = (object) value;
    }

    [DefaultSettingValue("0, 0")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public Size FormSize
    {
      get => (Size) this[nameof (FormSize)];
      set => this[nameof (FormSize)] = (object) value;
    }

    [DefaultSettingValue("0")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int SplitterDistance
    {
      get => (int) this[nameof (SplitterDistance)];
      set => this[nameof (SplitterDistance)] = (object) value;
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("-1")]
    public int ColumnNameWidth
    {
      get => (int) this[nameof (ColumnNameWidth)];
      set => this[nameof (ColumnNameWidth)] = (object) value;
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("-1")]
    public int ColumnLastModifiedWidth
    {
      get => (int) this[nameof (ColumnLastModifiedWidth)];
      set => this[nameof (ColumnLastModifiedWidth)] = (object) value;
    }

    [DefaultSettingValue("-1")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int ColumnSizeWidth
    {
      get => (int) this[nameof (ColumnSizeWidth)];
      set => this[nameof (ColumnSizeWidth)] = (object) value;
    }

    [DefaultSettingValue("-1")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public int ColumnNamePosition
    {
      get => (int) this[nameof (ColumnNamePosition)];
      set => this[nameof (ColumnNamePosition)] = (object) value;
    }

    [DefaultSettingValue("-1")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int ColumnLastModifiedPosition
    {
      get => (int) this[nameof (ColumnLastModifiedPosition)];
      set => this[nameof (ColumnLastModifiedPosition)] = (object) value;
    }

    [UserScopedSetting]
    [DefaultSettingValue("-1")]
    [DebuggerNonUserCode]
    public int ColumnSizePosition
    {
      get => (int) this[nameof (ColumnSizePosition)];
      set => this[nameof (ColumnSizePosition)] = (object) value;
    }

    [UserScopedSetting]
    [DefaultSettingValue("0")]
    [DebuggerNonUserCode]
    public int SortColumn
    {
      get => (int) this[nameof (SortColumn)];
      set => this[nameof (SortColumn)] = (object) value;
    }

    [DefaultSettingValue("Ascending")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public SortOrder SortColumnOrder
    {
      get => (SortOrder) this[nameof (SortColumnOrder)];
      set => this[nameof (SortColumnOrder)] = (object) value;
    }
  }
}
