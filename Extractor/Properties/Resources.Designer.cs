﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.Properties.Resources
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Nexon.CSO.Extractor.Properties
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Nexon.CSO.Extractor.Properties.Resources.resourceMan, (object) null))
          Nexon.CSO.Extractor.Properties.Resources.resourceMan = new ResourceManager("Nexon.CSO.Extractor.Properties.Resources", typeof (Nexon.CSO.Extractor.Properties.Resources).Assembly);
        return Nexon.CSO.Extractor.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => Nexon.CSO.Extractor.Properties.Resources.resourceCulture;
      set => Nexon.CSO.Extractor.Properties.Resources.resourceCulture = value;
    }
  }
}
