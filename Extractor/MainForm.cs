﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.MainForm
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using Nexon.CSO.Extractor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  public class MainForm : Form
  {
    private IContainer components;
    private MenuStrip menuStrip;
    private ToolStripMenuItem fileToolStripMenuItem;
    private ToolStripMenuItem newToolStripMenuItem;
    private ToolStripMenuItem openToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator;
    private ToolStripMenuItem saveToolStripMenuItem;
    private ToolStripMenuItem saveAsToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem exitToolStripMenuItem;
    private ToolStripMenuItem editToolStripMenuItem;
    private ToolStripMenuItem undoToolStripMenuItem;
    private ToolStripMenuItem redoToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem cutToolStripMenuItem;
    private ToolStripMenuItem copyToolStripMenuItem;
    private ToolStripMenuItem pasteToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripMenuItem selectAllToolStripMenuItem;
    private ToolStripMenuItem toolsToolStripMenuItem;
    private ToolStripMenuItem helpToolStripMenuItem;
    private ToolStripMenuItem aboutToolStripMenuItem;
    private StatusStrip statusStrip;
    private SplitContainer splitContainer1;
    private FolderTreeView treeView;
    private FilesListView listView;
    private OpenFileDialog narOpenFileDialog;
    private ToolStripStatusLabel pathToolStripStatusLabel;
    private ToolStripStatusLabel fileToolStripStatusLabel;
    private FolderBrowserDialog extractFolderBrowserDialog;
    private ToolStripMenuItem autoDecryptModelsToolStripMenuItem;
    private ToolStripSeparator toolStripMenuItem1;
    private ToolStripMenuItem extractAllToolStripMenuItem;
    private ToolStripMenuItem verifyAllToolStripMenuItem;
    private NexonArchive archive;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (MainForm));
      this.menuStrip = new MenuStrip();
      this.fileToolStripMenuItem = new ToolStripMenuItem();
      this.newToolStripMenuItem = new ToolStripMenuItem();
      this.openToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator = new ToolStripSeparator();
      this.saveToolStripMenuItem = new ToolStripMenuItem();
      this.saveAsToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.exitToolStripMenuItem = new ToolStripMenuItem();
      this.editToolStripMenuItem = new ToolStripMenuItem();
      this.undoToolStripMenuItem = new ToolStripMenuItem();
      this.redoToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.cutToolStripMenuItem = new ToolStripMenuItem();
      this.copyToolStripMenuItem = new ToolStripMenuItem();
      this.pasteToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator4 = new ToolStripSeparator();
      this.selectAllToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripMenuItem1 = new ToolStripSeparator();
      this.extractAllToolStripMenuItem = new ToolStripMenuItem();
      this.verifyAllToolStripMenuItem = new ToolStripMenuItem();
      this.toolsToolStripMenuItem = new ToolStripMenuItem();
      this.autoDecryptModelsToolStripMenuItem = new ToolStripMenuItem();
      this.helpToolStripMenuItem = new ToolStripMenuItem();
      this.aboutToolStripMenuItem = new ToolStripMenuItem();
      this.statusStrip = new StatusStrip();
      this.pathToolStripStatusLabel = new ToolStripStatusLabel();
      this.fileToolStripStatusLabel = new ToolStripStatusLabel();
      this.splitContainer1 = new SplitContainer();
      this.treeView = new FolderTreeView();
      this.listView = new FilesListView();
      this.narOpenFileDialog = new OpenFileDialog();
      this.extractFolderBrowserDialog = new FolderBrowserDialog();
      this.menuStrip.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      this.menuStrip.Items.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.fileToolStripMenuItem,
        (ToolStripItem) this.editToolStripMenuItem,
        (ToolStripItem) this.toolsToolStripMenuItem,
        (ToolStripItem) this.helpToolStripMenuItem
      });
      this.menuStrip.Location = new Point(0, 0);
      this.menuStrip.Name = "menuStrip";
      this.menuStrip.Size = new Size(624, 24);
      this.menuStrip.TabIndex = 0;
      this.menuStrip.Text = "menuStrip1";
      this.fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.newToolStripMenuItem,
        (ToolStripItem) this.openToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator,
        (ToolStripItem) this.saveToolStripMenuItem,
        (ToolStripItem) this.saveAsToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.exitToolStripMenuItem
      });
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      this.newToolStripMenuItem.Enabled = false;
      this.newToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("newToolStripMenuItem.Image");
      this.newToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.newToolStripMenuItem.Name = "newToolStripMenuItem";
      this.newToolStripMenuItem.ShortcutKeys = Keys.N | Keys.Control;
      this.newToolStripMenuItem.Size = new Size(146, 22);
      this.newToolStripMenuItem.Text = "&New";
      this.openToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("openToolStripMenuItem.Image");
      this.openToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.openToolStripMenuItem.Name = "openToolStripMenuItem";
      this.openToolStripMenuItem.ShortcutKeys = Keys.O | Keys.Control;
      this.openToolStripMenuItem.Size = new Size(146, 22);
      this.openToolStripMenuItem.Text = "&Open";
      this.openToolStripMenuItem.Click += new EventHandler(this.openToolStripMenuItem_Click);
      this.toolStripSeparator.Name = "toolStripSeparator";
      this.toolStripSeparator.Size = new Size(143, 6);
      this.saveToolStripMenuItem.Enabled = false;
      this.saveToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("saveToolStripMenuItem.Image");
      this.saveToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
      this.saveToolStripMenuItem.ShortcutKeys = Keys.S | Keys.Control;
      this.saveToolStripMenuItem.Size = new Size(146, 22);
      this.saveToolStripMenuItem.Text = "&Save";
      this.saveAsToolStripMenuItem.Enabled = false;
      this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
      this.saveAsToolStripMenuItem.Size = new Size(146, 22);
      this.saveAsToolStripMenuItem.Text = "Save &As";
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(143, 6);
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new Size(146, 22);
      this.exitToolStripMenuItem.Text = "E&xit";
      this.exitToolStripMenuItem.Click += new EventHandler(this.exitToolStripMenuItem_Click);
      this.editToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[11]
      {
        (ToolStripItem) this.undoToolStripMenuItem,
        (ToolStripItem) this.redoToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.cutToolStripMenuItem,
        (ToolStripItem) this.copyToolStripMenuItem,
        (ToolStripItem) this.pasteToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator4,
        (ToolStripItem) this.selectAllToolStripMenuItem,
        (ToolStripItem) this.toolStripMenuItem1,
        (ToolStripItem) this.extractAllToolStripMenuItem,
        (ToolStripItem) this.verifyAllToolStripMenuItem
      });
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      this.editToolStripMenuItem.Size = new Size(39, 20);
      this.editToolStripMenuItem.Text = "&Edit";
      this.undoToolStripMenuItem.Enabled = false;
      this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
      this.undoToolStripMenuItem.ShortcutKeys = Keys.Z | Keys.Control;
      this.undoToolStripMenuItem.Size = new Size(164, 22);
      this.undoToolStripMenuItem.Text = "&Undo";
      this.redoToolStripMenuItem.Enabled = false;
      this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
      this.redoToolStripMenuItem.ShortcutKeys = Keys.Y | Keys.Control;
      this.redoToolStripMenuItem.Size = new Size(164, 22);
      this.redoToolStripMenuItem.Text = "&Redo";
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(161, 6);
      this.cutToolStripMenuItem.Enabled = false;
      this.cutToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("cutToolStripMenuItem.Image");
      this.cutToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
      this.cutToolStripMenuItem.ShortcutKeys = Keys.X | Keys.Control;
      this.cutToolStripMenuItem.Size = new Size(164, 22);
      this.cutToolStripMenuItem.Text = "Cu&t";
      this.copyToolStripMenuItem.Enabled = false;
      this.copyToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("copyToolStripMenuItem.Image");
      this.copyToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
      this.copyToolStripMenuItem.ShortcutKeys = Keys.C | Keys.Control;
      this.copyToolStripMenuItem.Size = new Size(164, 22);
      this.copyToolStripMenuItem.Text = "&Copy";
      this.pasteToolStripMenuItem.Enabled = false;
      this.pasteToolStripMenuItem.Image = (Image) componentResourceManager.GetObject("pasteToolStripMenuItem.Image");
      this.pasteToolStripMenuItem.ImageTransparentColor = Color.Magenta;
      this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
      this.pasteToolStripMenuItem.ShortcutKeys = Keys.V | Keys.Control;
      this.pasteToolStripMenuItem.Size = new Size(164, 22);
      this.pasteToolStripMenuItem.Text = "&Paste";
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new Size(161, 6);
      this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
      this.selectAllToolStripMenuItem.ShortcutKeys = Keys.A | Keys.Control;
      this.selectAllToolStripMenuItem.Size = new Size(164, 22);
      this.selectAllToolStripMenuItem.Text = "Select &All";
      this.selectAllToolStripMenuItem.Click += new EventHandler(this.selectAllToolStripMenuItem_Click);
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new Size(161, 6);
      this.extractAllToolStripMenuItem.Name = "extractAllToolStripMenuItem";
      this.extractAllToolStripMenuItem.Size = new Size(164, 22);
      this.extractAllToolStripMenuItem.Text = "E&xtract All...";
      this.extractAllToolStripMenuItem.Click += new EventHandler(this.extractAllToolStripMenuItem_Click);
      this.verifyAllToolStripMenuItem.Name = "verifyAllToolStripMenuItem";
      this.verifyAllToolStripMenuItem.Size = new Size(164, 22);
      this.verifyAllToolStripMenuItem.Text = "&Verify All";
      this.verifyAllToolStripMenuItem.Click += new EventHandler(this.verifyAllToolStripMenuItem_Click);
      this.toolsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.autoDecryptModelsToolStripMenuItem
      });
      this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
      this.toolsToolStripMenuItem.Size = new Size(48, 20);
      this.toolsToolStripMenuItem.Text = "&Tools";
      this.autoDecryptModelsToolStripMenuItem.CheckOnClick = true;
      this.autoDecryptModelsToolStripMenuItem.Name = "autoDecryptModelsToolStripMenuItem";
      this.autoDecryptModelsToolStripMenuItem.Size = new Size(234, 22);
      this.autoDecryptModelsToolStripMenuItem.Text = "Automatically Decrypt Models";
      this.autoDecryptModelsToolStripMenuItem.Click += new EventHandler(this.autoDecryptModelsToolStripMenuItem_Click);
      this.helpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.aboutToolStripMenuItem
      });
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      this.helpToolStripMenuItem.Size = new Size(44, 20);
      this.helpToolStripMenuItem.Text = "&Help";
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new Size(116, 22);
      this.aboutToolStripMenuItem.Text = "&About...";
      this.aboutToolStripMenuItem.Click += new EventHandler(this.aboutToolStripMenuItem_Click);
      this.statusStrip.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.pathToolStripStatusLabel,
        (ToolStripItem) this.fileToolStripStatusLabel
      });
      this.statusStrip.Location = new Point(0, 422);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.RenderMode = ToolStripRenderMode.ManagerRenderMode;
      this.statusStrip.Size = new Size(624, 22);
      this.statusStrip.TabIndex = 2;
      this.pathToolStripStatusLabel.Name = "pathToolStripStatusLabel";
      this.pathToolStripStatusLabel.Size = new Size(478, 17);
      this.pathToolStripStatusLabel.Spring = true;
      this.pathToolStripStatusLabel.Text = "pathToolStripStatusLabel";
      this.pathToolStripStatusLabel.TextAlign = ContentAlignment.MiddleLeft;
      this.fileToolStripStatusLabel.Name = "fileToolStripStatusLabel";
      this.fileToolStripStatusLabel.Size = new Size(131, 17);
      this.fileToolStripStatusLabel.Text = "fileToolStripStatusLabel";
      this.fileToolStripStatusLabel.TextAlign = ContentAlignment.MiddleRight;
      this.splitContainer1.Dock = DockStyle.Fill;
      this.splitContainer1.Location = new Point(0, 24);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Panel1.Controls.Add((Control) this.treeView);
      this.splitContainer1.Panel2.Controls.Add((Control) this.listView);
      this.splitContainer1.Size = new Size(624, 398);
      this.splitContainer1.SplitterDistance = 208;
      this.splitContainer1.TabIndex = 1;
      this.splitContainer1.TabStop = false;
      this.treeView.Dock = DockStyle.Fill;
      this.treeView.HideSelection = false;
      this.treeView.Location = new Point(0, 0);
      this.treeView.Name = "treeView";
      this.treeView.Size = new Size(208, 398);
      this.treeView.Sorted = true;
      this.treeView.TabIndex = 0;
      this.treeView.ExtractFolder += new EventHandler<FilesEventArgs>(this.ExtractFiles);
      this.treeView.ShowFolder += new EventHandler<FilesEventArgs>(this.treeView_ShowFolder);
      this.treeView.VerifyFolder += new EventHandler<FilesEventArgs>(this.VerifyFiles);
      this.listView.AllowColumnReorder = true;
      this.listView.Dock = DockStyle.Fill;
      this.listView.FullPath = (string) null;
      this.listView.FullRowSelect = true;
      this.listView.HideSelection = false;
      this.listView.Location = new Point(0, 0);
      this.listView.Name = "listView";
      this.listView.Size = new Size(412, 398);
      this.listView.SortColumn = -1;
      this.listView.SortColumnOrder = SortOrder.None;
      this.listView.TabIndex = 0;
      this.listView.UseCompatibleStateImageBehavior = false;
      this.listView.View = View.Details;
      this.listView.VerifyFiles += new EventHandler<FilesEventArgs>(this.VerifyFiles);
      this.listView.SelectedIndexChanged += new EventHandler(this.listView_SelectedIndexChanged);
      this.listView.ExtractFiles += new EventHandler<FilesEventArgs>(this.ExtractFiles);
      this.narOpenFileDialog.DefaultExt = "nar";
      this.narOpenFileDialog.Filter = "NAR files|*.nar|All files|*.*";
      this.extractFolderBrowserDialog.Description = "Select folder to extract files to. Note that the archive's folder structure will be replicated in whichever folder you select.";
      this.AllowDrop = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(624, 444);
      this.Controls.Add((Control) this.splitContainer1);
      this.Controls.Add((Control) this.statusStrip);
      this.Controls.Add((Control) this.menuStrip);
      this.MainMenuStrip = this.menuStrip;
      this.Name = "NARExtractorForm";
      this.Text = "NAR Extractor";
      this.Load += new EventHandler(this.NARExtractorForm_Load);
      this.Shown += new EventHandler(this.NARExtractorForm_Shown);
      this.DragDrop += new DragEventHandler(this.NARExtractorForm_DragDrop);
      this.FormClosed += new FormClosedEventHandler(this.NARExtractorForm_FormClosed);
      this.DragEnter += new DragEventHandler(this.NARExtractorForm_DragEnter);
      this.FormClosing += new FormClosingEventHandler(this.NARExtractorForm_FormClosing);
      this.menuStrip.ResumeLayout(false);
      this.menuStrip.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public MainForm() => this.InitializeComponent();

    public string InitialLoadFile { get; set; }

    private void NARExtractorForm_Load(object sender, EventArgs e)
    {
      // ISSUE: variable of a compiler-generated type
      Settings settings = Settings.Default;
      if (!settings.FormSize.IsEmpty)
        this.Size = settings.FormSize;
      if (settings.FormWindowState != FormWindowState.Minimized)
        this.WindowState = settings.FormWindowState;
      if (settings.SplitterDistance > 0)
        this.splitContainer1.SplitterDistance = settings.SplitterDistance;
      if (settings.ColumnNameWidth >= 0)
        this.listView.Columns[0].Width = settings.ColumnNameWidth;
      if (settings.ColumnNamePosition >= 0)
        this.listView.Columns[0].DisplayIndex = settings.ColumnNamePosition;
      if (settings.ColumnLastModifiedWidth >= 0)
        this.listView.Columns[1].Width = settings.ColumnLastModifiedWidth;
      if (settings.ColumnLastModifiedPosition >= 0)
        this.listView.Columns[1].DisplayIndex = settings.ColumnLastModifiedPosition;
      if (settings.ColumnSizeWidth >= 0)
        this.listView.Columns[2].Width = settings.ColumnSizeWidth;
      if (settings.ColumnSizePosition >= 0)
        this.listView.Columns[2].DisplayIndex = settings.ColumnSizePosition;
      if (settings.SortColumn >= 0)
      {
        this.listView.SortColumn = settings.SortColumn;
        this.listView.SortColumnOrder = SortOrder.None;
      }
      if (settings.SortColumnOrder != SortOrder.None)
        this.listView.SortColumnOrder = settings.SortColumnOrder;
      this.autoDecryptModelsToolStripMenuItem.Checked = settings.AutoDecryptModels;
      this.pathToolStripStatusLabel.Text = string.Empty;
      this.fileToolStripStatusLabel.Text = string.Empty;
    }

    private void NARExtractorForm_Shown(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(this.InitialLoadFile))
        return;
      this.Open(this.InitialLoadFile);
    }

    private void NARExtractorForm_FormClosing(object sender, FormClosingEventArgs e) => this.CloseArchive();

    private void NARExtractorForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      // ISSUE: variable of a compiler-generated type
      Settings settings = Settings.Default;
      settings.FormSize = this.WindowState == FormWindowState.Minimized || this.WindowState == FormWindowState.Maximized ? this.RestoreBounds.Size : this.Size;
      settings.FormWindowState = this.WindowState;
      settings.SplitterDistance = this.splitContainer1.SplitterDistance;
      settings.ColumnNameWidth = this.listView.Columns[0].Width;
      settings.ColumnNamePosition = this.listView.Columns[0].DisplayIndex;
      settings.ColumnLastModifiedWidth = this.listView.Columns[1].Width;
      settings.ColumnLastModifiedPosition = this.listView.Columns[1].DisplayIndex;
      settings.ColumnSizeWidth = this.listView.Columns[2].Width;
      settings.ColumnSizePosition = this.listView.Columns[2].DisplayIndex;
      settings.SortColumn = this.listView.SortColumn;
      settings.SortColumnOrder = this.listView.SortColumnOrder;
      settings.AutoDecryptModels = this.autoDecryptModelsToolStripMenuItem.Checked;
      settings.Save();
    }

    private void openToolStripMenuItem_Click(object sender, EventArgs e) => this.OpenDialog();

    private void exitToolStripMenuItem_Click(object sender, EventArgs e) => this.Close();

    private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.listView.Focus();
      foreach (ListViewItem listViewItem in this.listView.Items)
        listViewItem.Selected = true;
    }

    private void extractAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.archive == null || this.treeView.TopNode == null)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      FolderTreeView.GetFilesRecursive(this.treeView.TopNode, (IList<NexonArchiveFileEntry>) archiveFileEntryList);
      this.ExtractFiles((object) this, new FilesEventArgs(FolderTreeView.GetFullPath(this.treeView.TopNode), (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    private void verifyAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.archive == null || this.treeView.TopNode == null)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      FolderTreeView.GetFilesRecursive(this.treeView.TopNode, (IList<NexonArchiveFileEntry>) archiveFileEntryList);
      this.VerifyFiles((object) this, new FilesEventArgs(FolderTreeView.GetFullPath(this.treeView.TopNode), (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    private void autoDecryptModelsToolStripMenuItem_Click(object sender, EventArgs e) => Settings.Default.AutoDecryptModels = this.autoDecryptModelsToolStripMenuItem.Checked;

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      using (AboutBox aboutBox = new AboutBox())
      {
        int num = (int) aboutBox.ShowDialog();
      }
    }

    private void NARExtractorForm_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
        e.Effect = DragDropEffects.Move;
      else
        e.Effect = DragDropEffects.None;
    }

    private void NARExtractorForm_DragDrop(object sender, DragEventArgs e)
    {
      if (!e.Data.GetDataPresent(DataFormats.FileDrop) || !(e.Data.GetData(DataFormats.FileDrop) is string[] data) || data.Length <= 0)
        return;
      this.Open(data[0]);
    }

    private void treeView_ShowFolder(object sender, FilesEventArgs e)
    {
      this.pathToolStripStatusLabel.Text = e.Path;
      this.listView.FullPath = e.Path;
      this.listView.BeginUpdate();
      this.listView.Items.Clear();
      this.listView_SelectedIndexChanged((object) this, EventArgs.Empty);
      if (e.Files != null)
      {
        foreach (NexonArchiveFileEntry file in (IEnumerable<NexonArchiveFileEntry>) e.Files)
          this.listView.AddFile(file);
      }
      this.listView.EndUpdate();
      this.listView_SelectedIndexChanged((object) this, EventArgs.Empty);
    }

    private void listView_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.treeView.SelectedNode != null)
      {
        if (this.listView.SelectedIndices.Count > 0)
        {
          if (this.listView.SelectedIndices.Count == 1)
            this.fileToolStripStatusLabel.Text = string.Format((IFormatProvider) NumberFormatInfo.CurrentInfo, "{0} item selected", (object) this.listView.SelectedIndices.Count);
          else
            this.fileToolStripStatusLabel.Text = string.Format((IFormatProvider) NumberFormatInfo.CurrentInfo, "{0} items selected", (object) this.listView.SelectedIndices.Count);
        }
        else if (this.listView.Items.Count == 1)
          this.fileToolStripStatusLabel.Text = string.Format((IFormatProvider) NumberFormatInfo.CurrentInfo, "{0} item", (object) this.listView.Items.Count);
        else
          this.fileToolStripStatusLabel.Text = string.Format((IFormatProvider) NumberFormatInfo.CurrentInfo, "{0} items", (object) this.listView.Items.Count);
      }
      else
        this.fileToolStripStatusLabel.Text = string.Empty;
    }

    private void SetTitle(string fileName) => this.SetTitle(fileName, false);

    private void SetTitle(string fileName, bool modified)
    {
      if (!string.IsNullOrEmpty(fileName))
      {
        if (modified)
          this.Text = string.Format((IFormatProvider) CultureInfo.CurrentUICulture, "NAR Extractor [{0}]*", (object) fileName);
        else
          this.Text = string.Format((IFormatProvider) CultureInfo.CurrentUICulture, "NAR Extractor [{0}]", (object) fileName);
      }
      else
        this.Text = string.Format((IFormatProvider) CultureInfo.CurrentUICulture, "NAR Extractor");
    }

    private void CloseArchive()
    {
      if (this.archive == null)
        return;
      this.treeView.Nodes.Clear();
      this.listView.Items.Clear();
      this.archive.Close();
      this.SetTitle((string) null);
    }

    private void OpenDialog()
    {
      if (this.narOpenFileDialog.ShowDialog() != DialogResult.OK)
        return;
      this.Open(this.narOpenFileDialog.FileName);
    }

    public void Open(string fileName)
    {
      this.CloseArchive();
      try
      {
        this.archive = new NexonArchive();
        this.archive.Load(fileName, false);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show((IWin32Window) this, "Could not open file: " + fileName, "Error");
        return;
      }
      this.SetTitle(fileName);
      this.treeView.LoadArchive(this.archive);
    }

    private void ExtractFiles(object sender, FilesEventArgs e)
    {
      if (this.extractFolderBrowserDialog.ShowDialog() != DialogResult.OK)
        return;
      MainForm.ExtractHelper extractHelper = new MainForm.ExtractHelper();
      extractHelper.DecryptModels = Settings.Default.AutoDecryptModels;
      extractHelper.ExtractPath = this.extractFolderBrowserDialog.SelectedPath;
      using (TaskDialog taskDialog = new TaskDialog(new Predicate<NexonArchiveFileEntry>(extractHelper.Extract), (ICollection<NexonArchiveFileEntry>) e.Files))
      {
        taskDialog.Text = "Extracting files...";
        taskDialog.DestinationPath = extractHelper.ExtractPath;
        switch (taskDialog.ShowDialog((IWin32Window) this))
        {
          case DialogResult.OK:
            int num1 = (int) MessageBox.Show("All the selected files have been extracted successfully.", "Extraction Complete");
            break;
          case DialogResult.Abort:
            int num2 = (int) MessageBox.Show("An error occured while extracting the files.", "Error");
            break;
        }
      }
    }

    private static bool VerifyFilesHelper(NexonArchiveFileEntry file) => file.Verify();

    private void VerifyFiles(object sender, FilesEventArgs e)
    {
      using (TaskDialog taskDialog = new TaskDialog(new Predicate<NexonArchiveFileEntry>(MainForm.VerifyFilesHelper), (ICollection<NexonArchiveFileEntry>) e.Files))
      {
        taskDialog.Text = "Verifying files...";
        switch (taskDialog.ShowDialog((IWin32Window) this))
        {
          case DialogResult.OK:
            int num1 = (int) MessageBox.Show("All the selected files have been verified successfully.", "Verification Complete");
            break;
          case DialogResult.Abort:
            int num2 = (int) MessageBox.Show("An error occured while verifying the files.", "Error");
            break;
        }
      }
    }

    private sealed class ExtractHelper
    {
      public bool DecryptModels;
      public string ExtractPath;

      public bool Extract(NexonArchiveFileEntry file)
      {
        string path1 = file.Path;
        int num1 = 0;
        while ((int) path1[num1] == (int) Path.DirectorySeparatorChar || (int) path1[num1] == (int) Path.AltDirectorySeparatorChar)
          ++num1;
        string path2 = Path.Combine(this.ExtractPath, path1.Substring(num1));
        DirectoryInfo directoryInfo = new DirectoryInfo(Path.GetDirectoryName(path2));
        if (!directoryInfo.Exists)
          directoryInfo.Create();
        using (FileStream fileStream = new FileStream(path2, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
        {
          file.Extract((Stream) fileStream);
          if (this.DecryptModels)
          {
            if (string.Compare(Path.GetExtension(path2), ".mdl", StringComparison.OrdinalIgnoreCase) == 0)
            {
              int num2 = (int) ModelHelper.DecryptModel((Stream) fileStream);
            }
          }
        }
        return true;
      }
    }
  }
}
