﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.Program
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  internal static class Program
  {
    [STAThread]
    private static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      MainForm mainForm = new MainForm();
      if (args.Length > 0)
        mainForm.InitialLoadFile = args[0];
      Application.Run((Form) mainForm);
    }
  }
}
