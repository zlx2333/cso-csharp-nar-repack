﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.FilesEventArgs
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System;
using System.Collections.Generic;

namespace Nexon.CSO.Extractor
{
  public class FilesEventArgs : EventArgs
  {
    private string path;
    private IList<NexonArchiveFileEntry> files;

    public FilesEventArgs()
      : this(string.Empty, (IList<NexonArchiveFileEntry>) null)
    {
    }

    public FilesEventArgs(string path, IList<NexonArchiveFileEntry> files)
    {
      this.path = path;
      this.files = files;
    }

    public string Path => this.path;

    public IList<NexonArchiveFileEntry> Files => this.files;
  }
}
