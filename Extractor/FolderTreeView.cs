﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.FolderTreeView
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  public class FolderTreeView : TreeView
  {
    private IContainer components;
    private ContextMenuStrip contextMenuStrip;
    private ToolStripMenuItem extractToolStripMenuItem;
    private ToolStripMenuItem verifyToolStripMenuItem;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.contextMenuStrip = new ContextMenuStrip(this.components);
      this.extractToolStripMenuItem = new ToolStripMenuItem();
      this.verifyToolStripMenuItem = new ToolStripMenuItem();
      this.contextMenuStrip.SuspendLayout();
      this.SuspendLayout();
      this.contextMenuStrip.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.extractToolStripMenuItem,
        (ToolStripItem) this.verifyToolStripMenuItem
      });
      this.contextMenuStrip.Name = "contextMenuStrip";
      this.contextMenuStrip.Size = new Size(119, 48);
      this.contextMenuStrip.Opening += new CancelEventHandler(this.contextMenuStrip_Opening);
      this.extractToolStripMenuItem.Name = "extractToolStripMenuItem";
      this.extractToolStripMenuItem.Size = new Size(118, 22);
      this.extractToolStripMenuItem.Text = "E&xtract...";
      this.extractToolStripMenuItem.Click += new EventHandler(this.extractToolStripMenuItem_Click);
      this.verifyToolStripMenuItem.Name = "verifyToolStripMenuItem";
      this.verifyToolStripMenuItem.Size = new Size(118, 22);
      this.verifyToolStripMenuItem.Text = "&Verify";
      this.verifyToolStripMenuItem.Click += new EventHandler(this.verifyToolStripMenuItem_Click);
      this.ContextMenuStrip = this.contextMenuStrip;
      this.HideSelection = false;
      this.LineColor = Color.Black;
      this.AfterSelect += new TreeViewEventHandler(this.FolderTreeView_AfterSelect);
      this.MouseDown += new MouseEventHandler(this.FolderTreeView_MouseDown);
      this.contextMenuStrip.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public event EventHandler<FilesEventArgs> ShowFolder;

    public event EventHandler<FilesEventArgs> ExtractFolder;

    public event EventHandler<FilesEventArgs> VerifyFolder;

    public FolderTreeView()
    {
      this.InitializeComponent();
      this.TreeViewNodeSorter = (IComparer) new FolderTreeView.TreeSort(this);
    }

    protected void OnShowFolder(FilesEventArgs e)
    {
      if (e == null)
        throw new ArgumentNullException(nameof (e));
      if (this.ShowFolder == null)
        return;
      this.ShowFolder((object) this, e);
    }

    protected void OnExtractFolder(FilesEventArgs e)
    {
      if (e == null)
        throw new ArgumentNullException(nameof (e));
      if (this.ExtractFolder == null)
        return;
      this.ExtractFolder((object) this, e);
    }

    protected void OnVerifyFolder(FilesEventArgs e)
    {
      if (e == null)
        throw new ArgumentNullException(nameof (e));
      if (this.VerifyFolder == null)
        return;
      this.VerifyFolder((object) this, e);
    }

    private void FolderTreeView_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left && e.Button != MouseButtons.Right)
        return;
      this.SelectedNode = this.GetNodeAt(e.Location);
      if (this.SelectedNode != null)
        return;
      this.OnShowFolder(new FilesEventArgs());
    }

    private void FolderTreeView_AfterSelect(object sender, TreeViewEventArgs e) => this.OnShowFolder(new FilesEventArgs(FolderTreeView.GetFullPath(e.Node), (IList<NexonArchiveFileEntry>) (e.Node.Tag as List<NexonArchiveFileEntry>)));

    private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
    {
      if (this.SelectedNode != null)
        return;
      e.Cancel = true;
    }

    private void extractToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.SelectedNode == null)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      FolderTreeView.GetFilesRecursive(this.SelectedNode, (IList<NexonArchiveFileEntry>) archiveFileEntryList);
      this.OnExtractFolder(new FilesEventArgs(FolderTreeView.GetFullPath(this.SelectedNode), (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    private void verifyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.SelectedNode == null)
        return;
      List<NexonArchiveFileEntry> archiveFileEntryList = new List<NexonArchiveFileEntry>();
      FolderTreeView.GetFilesRecursive(this.SelectedNode, (IList<NexonArchiveFileEntry>) archiveFileEntryList);
      this.OnVerifyFolder(new FilesEventArgs(FolderTreeView.GetFullPath(this.SelectedNode), (IList<NexonArchiveFileEntry>) archiveFileEntryList));
    }

    private static TreeNode FindOrCreateNodePath(TreeNode rootNode, string path)
    {
      if (path.Length == 0)
        return rootNode;
      int num1 = 0;
      while (path[num1] == '/' || path[num1] == '\\')
        ++num1;
      int num2 = path.IndexOfAny(new char[2]{ '/', '\\' }, num1);
      string str = num2 < 0 ? path.Substring(num1) : path.Substring(num1, num2 - num1);
      TreeNode treeNode = (TreeNode) null;
      foreach (TreeNode node in rootNode.Nodes)
      {
        if (string.Compare(node.Name, str, StringComparison.OrdinalIgnoreCase) == 0)
        {
          treeNode = node;
          break;
        }
      }
      if (treeNode == null)
      {
        treeNode = new TreeNode(str);
        treeNode.Name = str;
        rootNode.Nodes.Add(treeNode);
      }
      return num2 >= 0 ? FolderTreeView.FindOrCreateNodePath(treeNode, path.Substring(num2 + 1)) : treeNode;
    }

    public void LoadArchive(NexonArchive archive)
    {
      this.Nodes.Clear();
      if (archive == null)
        return;
      TreeNode treeNode = new TreeNode("(root)");
      foreach (NexonArchiveFileEntry fileEntry in archive.FileEntries)
      {
        TreeNode orCreateNodePath = FolderTreeView.FindOrCreateNodePath(treeNode, Path.GetDirectoryName(fileEntry.Path));
        if (!(orCreateNodePath.Tag is IList<NexonArchiveFileEntry> archiveFileEntryList))
        {
          archiveFileEntryList = (IList<NexonArchiveFileEntry>) new List<NexonArchiveFileEntry>();
          orCreateNodePath.Tag = (object) archiveFileEntryList;
        }
        archiveFileEntryList.Add(fileEntry);
      }
      treeNode.Expand();
      this.Nodes.Add(treeNode);
      this.SelectedNode = treeNode;
    }

    public static IList<NexonArchiveFileEntry> GetFilesRecursive(
      TreeNode node,
      IList<NexonArchiveFileEntry> files)
    {
      if (node == null)
        throw new ArgumentNullException(nameof (node));
      if (files == null)
        throw new ArgumentNullException(nameof (files));
      if (node.Tag is IList<NexonArchiveFileEntry> tag)
      {
        foreach (NexonArchiveFileEntry archiveFileEntry in (IEnumerable<NexonArchiveFileEntry>) tag)
          files.Add(archiveFileEntry);
      }
      foreach (TreeNode node1 in node.Nodes)
        FolderTreeView.GetFilesRecursive(node1, files);
      return files;
    }

    public static string GetFullPath(TreeNode node)
    {
      if (node == null || node.Parent == null)
        return string.Empty;
      return node.Parent != null && node.Parent.Parent != null ? FolderTreeView.GetFullPath(node.Parent) + "/" + node.Text : node.Text;
    }

    private sealed class TreeSort : IComparer
    {
      private FolderTreeView treeView;

      public TreeSort(FolderTreeView treeView) => this.treeView = treeView;

      public int Compare(object x, object y)
      {
        TreeNode treeNode1 = x as TreeNode;
        TreeNode treeNode2 = y as TreeNode;
        return treeNode1 == null ? (treeNode2 == null ? 0 : -1) : (treeNode2 == null ? 1 : string.Compare(treeNode1.Text, treeNode2.Text, StringComparison.CurrentCultureIgnoreCase));
      }
    }
  }
}
