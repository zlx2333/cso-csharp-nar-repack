﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Extractor.PathLabel
// Assembly: Extractor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A592CDD5-C453-48BE-9EA3-9997831E1DE9
// Assembly location: D:\PortablePrograms\CSO Modding Toolkit\CSO Tools v2.0\Extractor.exe

using System.Drawing;
using System.Windows.Forms;

namespace Nexon.CSO.Extractor
{
  public class PathLabel : Label
  {
    private StringFormat stringFormat = new StringFormat(StringFormatFlags.NoWrap);
    private SolidBrush foreBrush;

    public PathLabel()
    {
      this.stringFormat.Trimming = StringTrimming.EllipsisPath;
      this.foreBrush = new SolidBrush(this.ForeColor);
    }

    public override Color ForeColor
    {
      get => base.ForeColor;
      set
      {
        base.ForeColor = value;
        this.foreBrush.Dispose();
        this.foreBrush = new SolidBrush(value);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        this.stringFormat.Dispose();
        this.foreBrush.Dispose();
      }
      base.Dispose(disposing);
    }

    protected override void OnPaint(PaintEventArgs e) => e.Graphics.DrawString(this.Text, this.Font, (Brush) this.foreBrush, (RectangleF) this.ClientRectangle, this.stringFormat);
  }
}
