﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.ModelHelper
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.IO;
using System.Security.Cryptography;

namespace Nexon.CSO
{
  public static class ModelHelper
  {
    private static readonly byte[] Version20Key = new byte[32]
    {
      (byte) 50,
      (byte) 166,
      (byte) 33,
      (byte) 224,
      (byte) 171,
      (byte) 107,
      (byte) 244,
      (byte) 44,
      (byte) 147,
      (byte) 198,
      (byte) 241,
      (byte) 150,
      (byte) 251,
      (byte) 56,
      (byte) 117,
      (byte) 104,
      (byte) 186,
      (byte) 112,
      (byte) 19,
      (byte) 134,
      (byte) 224,
      (byte) 179,
      (byte) 113,
      (byte) 244,
      (byte) 227,
      (byte) 155,
      (byte) 7,
      (byte) 34,
      (byte) 12,
      (byte) 254,
      (byte) 136,
      (byte) 58
    };
    private static readonly byte[] Version21Key = new byte[32]
    {
      (byte) 34,
      (byte) 122,
      (byte) 25,
      (byte) 111,
      (byte) 123,
      (byte) 134,
      (byte) 125,
      (byte) 224,
      (byte) 140,
      (byte) 198,
      (byte) 241,
      (byte) 150,
      (byte) 251,
      (byte) 56,
      (byte) 117,
      (byte) 104,
      (byte) 136,
      (byte) 122,
      (byte) 120,
      (byte) 134,
      (byte) 120,
      (byte) 134,
      (byte) 103,
      (byte) 112,
      (byte) 217,
      (byte) 145,
      (byte) 7,
      (byte) 58,
      (byte) 20,
      (byte) 116,
      (byte) 254,
      (byte) 34
    };

    private static void TransformChunk(
      ICryptoTransform transform,
      byte[] data,
      int offset,
      int length)
    {
      if (length == 0)
        return;
      while (length > 0)
      {
        int inputCount = length;
        if (inputCount > 1024)
          inputCount = 1024;
        if ((inputCount & 7) != 0 || inputCount == 0)
          break;
        int num = transform.TransformBlock(data, offset, inputCount, data, offset);
        length -= num;
        offset += num;
      }
    }

    public static ModelResult DecryptModel(string path)
    {
      using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
        return ModelHelper.DecryptModel((Stream) fileStream);
    }

    public static ModelResult DecryptModel(Stream stream)
    {
      if (stream == null)
        throw new ArgumentNullException(nameof (stream));
      if (!stream.CanRead)
        throw new ArgumentException("Cannot read from stream.", nameof (stream));
      if (!stream.CanWrite)
        throw new ArgumentException("Cannot write to stream.", nameof (stream));
      if (!stream.CanSeek)
        throw new ArgumentException("Cannot seek in stream.", nameof (stream));
      stream.Seek(0L, SeekOrigin.Begin);
      if (stream.Length > (long) int.MaxValue || stream.Length < 244L)
        return ModelResult.InvalidModel;
      BinaryReader binaryReader = new BinaryReader(stream);
      BinaryWriter binaryWriter = new BinaryWriter(stream);
      int num1;
      try
      {
        if (binaryReader.ReadInt32() != 1414743113)
          return ModelResult.InvalidModel;
        num1 = binaryReader.ReadInt32();
        stream.Seek(72L, SeekOrigin.Begin);
        if ((long) binaryReader.ReadInt32() < stream.Length)
          return ModelResult.InvalidModel;
      }
      catch (EndOfStreamException ex)
      {
        return ModelResult.InvalidModel;
      }
      SymmetricAlgorithm symmetricAlgorithm;
      switch (num1)
      {
        case 20:
          symmetricAlgorithm = (SymmetricAlgorithm) new Ice(4);
          symmetricAlgorithm.Key = ModelHelper.Version20Key;
          break;
        case 21:
          symmetricAlgorithm = (SymmetricAlgorithm) new Ice(4);
          symmetricAlgorithm.Key = ModelHelper.Version21Key;
          break;
        default:
          return ModelResult.NotEncrypted;
      }
      using (symmetricAlgorithm)
      {
        using (ICryptoTransform decryptor = symmetricAlgorithm.CreateDecryptor())
        {
          stream.Seek(4L, SeekOrigin.Begin);
          binaryWriter.Write(9);
          try
          {
            stream.Seek(180L, SeekOrigin.Begin);
            int num2 = binaryReader.ReadInt32();
            int num3 = binaryReader.ReadInt32();
            int num4 = 0;
            while (num4 < num2)
            {
              if (num3 >= 0)
              {
                try
                {
                  stream.Seek((long) (num3 + 68), SeekOrigin.Begin);
                  int num5 = binaryReader.ReadInt32();
                  int num6 = binaryReader.ReadInt32();
                  int num7 = binaryReader.ReadInt32();
                  if (num5 >= 0)
                  {
                    if (num6 >= 0)
                    {
                      if (num7 >= 0)
                      {
                        stream.Seek((long) num7, SeekOrigin.Begin);
                        byte[] numArray = binaryReader.ReadBytes(num5 * num6 + 768);
                        ModelHelper.TransformChunk(decryptor, numArray, 0, numArray.Length);
                        stream.Seek((long) num7, SeekOrigin.Begin);
                        binaryWriter.Write(numArray, 0, numArray.Length);
                      }
                    }
                  }
                }
                catch (EndOfStreamException ex)
                {
                }
                ++num4;
                num3 += 80;
              }
              else
                break;
            }
          }
          catch (EndOfStreamException ex)
          {
          }
          try
          {
            stream.Seek(204L, SeekOrigin.Begin);
            int num2 = binaryReader.ReadInt32();
            int num3 = binaryReader.ReadInt32();
            int num4 = 0;
            while (num4 < num2)
            {
              if (num3 >= 0)
              {
                try
                {
                  stream.Seek((long) (num3 + 64), SeekOrigin.Begin);
                  int num5 = binaryReader.ReadInt32();
                  stream.Seek(4L, SeekOrigin.Current);
                  int num6 = binaryReader.ReadInt32();
                  if (num6 >= 0)
                  {
                    int num7 = 0;
                    while (num7 < num5)
                    {
                      if (num6 >= 0)
                      {
                        try
                        {
                          stream.Seek((long) (num6 + 80), SeekOrigin.Begin);
                          int num8 = binaryReader.ReadInt32();
                          stream.Seek(4L, SeekOrigin.Current);
                          int num9 = binaryReader.ReadInt32();
                          if (num9 >= 0)
                          {
                            stream.Seek((long) num9, SeekOrigin.Begin);
                            byte[] numArray = binaryReader.ReadBytes(num8 * 12);
                            ModelHelper.TransformChunk(decryptor, numArray, 0, numArray.Length);
                            stream.Seek((long) num9, SeekOrigin.Begin);
                            binaryWriter.Write(numArray, 0, numArray.Length);
                          }
                        }
                        catch (EndOfStreamException ex)
                        {
                        }
                        ++num7;
                        num6 += 112;
                      }
                      else
                        break;
                    }
                  }
                }
                catch (EndOfStreamException ex)
                {
                }
                ++num4;
                num3 += 76;
              }
              else
                break;
            }
          }
          catch (EndOfStreamException ex)
          {
          }
        }
      }
      return ModelResult.Success;
    }
  }
}
