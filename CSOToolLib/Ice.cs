﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.Ice
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.Security.Cryptography;

namespace Nexon.CSO
{
  public sealed class Ice : SymmetricAlgorithm
  {
    private int n;

    public Ice()
      : this(0)
    {
    }

    public Ice(int n)
    {
      if (n < 0)
        throw new ArgumentOutOfRangeException(nameof (n));
      this.n = n;
      this.ModeValue = CipherMode.ECB;
      this.PaddingValue = PaddingMode.None;
      this.BlockSizeValue = 64;
      this.LegalBlockSizesValue = new KeySizes[1]
      {
        new KeySizes(this.BlockSizeValue, this.BlockSizeValue, 0)
      };
      this.KeySizeValue = Math.Max(n << 6, 64);
      this.LegalKeySizesValue = new KeySizes[1]
      {
        new KeySizes(this.KeySizeValue, this.KeySizeValue, 0)
      };
    }

    public override CipherMode Mode
    {
      get
      {
        return base.Mode;
      }
      set
      {
        if (value != CipherMode.ECB)
          throw new NotSupportedException("Only ECB is currently supported.");
        base.Mode = value;
      }
    }

    public override PaddingMode Padding
    {
      get
      {
        return base.Padding;
      }
      set
      {
        if (value != PaddingMode.None)
          throw new NotSupportedException("No padding is currently supported.");
        base.Padding = value;
      }
    }

    public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
    {
      if (rgbKey == null)
        throw new ArgumentNullException(nameof (rgbKey));
      if (rgbKey.Length != this.KeySizeValue >> 3)
        throw new ArgumentException("Key size is not valid.", nameof (rgbKey));
      return (ICryptoTransform) new IceCryptoTransform(this.n, rgbKey, false);
    }

    public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
    {
      if (rgbKey == null)
        throw new ArgumentNullException(nameof (rgbKey));
      if (rgbKey.Length != this.KeySizeValue >> 3)
        throw new ArgumentException("Key size is not valid.", nameof (rgbKey));
      return (ICryptoTransform) new IceCryptoTransform(this.n, rgbKey, true);
    }

    public override void GenerateIV()
    {
      RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();
      byte[] data = new byte[8];
      randomNumberGenerator.GetBytes(data);
      this.IVValue = data;
    }

    public override void GenerateKey()
    {
      RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();
      byte[] data = new byte[this.KeySizeValue >> 3];
      randomNumberGenerator.GetBytes(data);
      this.KeyValue = data;
    }
  }
}
