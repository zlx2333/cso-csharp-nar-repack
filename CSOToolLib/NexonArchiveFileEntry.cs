﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.NexonArchiveFileEntry
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using ICSharpCode.SharpZipLib.Checksums;
using System;
using System.IO;
using System.Text;

namespace Nexon.CSO
{
  public sealed class NexonArchiveFileEntry
  {
    private NexonArchive archive;
    private string path;
    private NexonArchiveFileEntryType storedType;
    private long offset;
    private long storedSize;
    private long extractedSize;
    private DateTime lastModifiedTime;
    private uint checksum;

    internal NexonArchiveFileEntry(NexonArchive archive)
    {
      this.archive = archive;
    }

    public NexonArchive Archive
    {
      get
      {
        return this.archive;
      }
    }

    public string Path
    {
      get
      {
        return this.path;
      }
    }

    public long Size
    {
      get
      {
        return this.extractedSize;
      }
    }

    public DateTime LastModifiedTime
    {
      get
      {
        return this.lastModifiedTime;
      }
    }

    private static DateTime FromEpoch(int epoch)
    {
      return new DateTime((long) epoch * 10000000L + 621355968000000000L);
    }

    internal int Load(byte[] header, int offset)
    {
      try
      {
        int uint16 = (int) BitConverter.ToUInt16(header, offset);
        this.path = Encoding.GetEncoding("euc-kr").GetString(header, offset + 2, uint16);
        this.storedType = (NexonArchiveFileEntryType) BitConverter.ToInt32(header, offset + 2 + uint16);
        this.offset = (long) BitConverter.ToUInt32(header, offset + 2 + uint16 + 4);
        this.storedSize = (long) BitConverter.ToInt32(header, offset + 2 + uint16 + 8);
        this.extractedSize = (long) BitConverter.ToInt32(header, offset + 2 + uint16 + 12);
        this.lastModifiedTime = NexonArchiveFileEntry.FromEpoch(BitConverter.ToInt32(header, offset + 2 + uint16 + 16));
        this.checksum = BitConverter.ToUInt32(header, offset + 2 + uint16 + 20);
        return 2 + uint16 + 24;
      }
      catch (ArgumentOutOfRangeException ex)
      {
        throw new InvalidDataException("NAR file entry is invalid.", (Exception) ex);
      }
    }

    public long Extract(Stream outputStream)
    {
      if (outputStream == null)
        throw new ArgumentNullException(nameof (outputStream));
      if (!outputStream.CanWrite)
        throw new ArgumentException("Cannot write to stream.", nameof (outputStream));
      if (this.extractedSize == 0L)
        return 0;
      lock (this.archive.Stream)
      {
        Stream stream = (Stream) new BoundedStream(this.archive.Stream, this.offset, this.storedSize);
        stream.Position = 0L;
        switch (this.storedType)
        {
          case NexonArchiveFileEntryType.Raw:
            lock (outputStream)
            {
              byte[] buffer = new byte[8192];
              long num = 0;
              int count;
              while ((count = stream.Read(buffer, 0, 8192)) > 0)
              {
                outputStream.Write(buffer, 0, count);
                num += (long) count;
              }
              return num;
            }
          case NexonArchiveFileEntryType.Encoded:
            stream = (Stream) new NexonArchiveFileDecoderStream(stream, this.path);
            goto case NexonArchiveFileEntryType.Raw;
          case NexonArchiveFileEntryType.EncodedAndCompressed:
            stream = (Stream) new NexonArchiveFileDecompressStream((Stream) new NexonArchiveFileDecoderStream(stream, this.path), this.extractedSize);
            goto case NexonArchiveFileEntryType.Raw;
          default:
            throw new NotSupportedException("Unsupported file storage type: " + (object) this.storedType + ".");
        }
      }
    }

    public bool Verify()
    {
      Crc32 crc32 = new Crc32();
      lock (this.archive.Stream)
      {
        Stream stream = (Stream) new BoundedStream(this.archive.Stream, this.offset, this.storedSize);
        stream.Position = 0L;
        byte[] buffer = new byte[8192];
        int count;
        while ((count = stream.Read(buffer, 0, 8192)) > 0)
          crc32.Update(buffer, 0, count);
      }
      return (long) this.checksum == crc32.Value;
    }
  }
}
