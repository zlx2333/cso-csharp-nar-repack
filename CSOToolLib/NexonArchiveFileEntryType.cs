﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.NexonArchiveFileEntryType
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

namespace Nexon.CSO
{
  public enum NexonArchiveFileEntryType
  {
    Raw,
    Encoded,
    EncodedAndCompressed,
  }
}
