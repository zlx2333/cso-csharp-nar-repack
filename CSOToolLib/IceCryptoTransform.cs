﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.IceCryptoTransform
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.Security.Cryptography;

namespace Nexon.CSO
{
  internal sealed class IceCryptoTransform : ICryptoTransform, IDisposable
  {
    private static readonly int[,] SMod = new int[4, 4]
    {
      {
        333,
        313,
        505,
        369
      },
      {
        379,
        375,
        319,
        391
      },
      {
        361,
        445,
        451,
        397
      },
      {
        397,
        425,
        395,
        505
      }
    };
    private static readonly int[,] SXor = new int[4, 4]
    {
      {
        131,
        133,
        155,
        205
      },
      {
        204,
        167,
        173,
        65
      },
      {
        75,
        46,
        212,
        51
      },
      {
        234,
        203,
        46,
        4
      }
    };
    private static readonly uint[] PBox = new uint[32]
    {
      1U,
      128U,
      1024U,
      8192U,
      524288U,
      2097152U,
      16777216U,
      1073741824U,
      8U,
      32U,
      256U,
      16384U,
      65536U,
      8388608U,
      67108864U,
      536870912U,
      4U,
      16U,
      512U,
      32768U,
      131072U,
      4194304U,
      134217728U,
      268435456U,
      2U,
      64U,
      2048U,
      4096U,
      262144U,
      1048576U,
      33554432U,
      2147483648U
    };
    private static readonly int[] KeyRotation = new int[16]
    {
      0,
      1,
      2,
      3,
      2,
      1,
      3,
      0,
      1,
      3,
      2,
      0,
      3,
      1,
      0,
      2
    };
    private static uint[,] SBox;
    private bool encrypt;
    private int size;
    private int rounds;
    private uint[,] keySchedule;

    internal IceCryptoTransform(int n, byte[] key, bool encrypt)
    {
      this.encrypt = encrypt;
      IceCryptoTransform.InitializeSBox();
      if (n == 0)
      {
        this.size = 1;
        this.rounds = 8;
      }
      else
      {
        this.size = n;
        this.rounds = n << 4;
      }
      this.keySchedule = new uint[this.rounds, 3];
      this.SetKey(key);
    }

    private static uint GFMultiply(uint a, uint b, uint m)
    {
      uint num = 0;
      while (b != 0U)
      {
        if (((int) b & 1) != 0)
          num ^= a;
        a <<= 1;
        b >>= 1;
        if (a >= 256U)
          a ^= m;
      }
      return num;
    }

    private static uint GFExp7(uint b, uint m)
    {
      if (b == 0U)
        return 0;
      uint b1 = IceCryptoTransform.GFMultiply(b, b, m);
      uint num = IceCryptoTransform.GFMultiply(b, b1, m);
      uint b2 = IceCryptoTransform.GFMultiply(num, num, m);
      return IceCryptoTransform.GFMultiply(b, b2, m);
    }

    private static uint Perm32(uint x)
    {
      uint num = 0;
      int index = 0;
      for (; x != 0U; x >>= 1)
      {
        if (((int) x & 1) != 0)
          num |= IceCryptoTransform.PBox[index];
        ++index;
      }
      return num;
    }

    private static void InitializeSBox()
    {
      if (IceCryptoTransform.SBox != null)
        return;
      IceCryptoTransform.SBox = new uint[4, 1024];
      for (int index1 = 0; index1 < 1024; ++index1)
      {
        int num = index1 >> 1 & (int) byte.MaxValue;
        int index2 = index1 & 1 | (index1 & 512) >> 8;
        IceCryptoTransform.SBox[0, index1] = IceCryptoTransform.Perm32(IceCryptoTransform.GFExp7((uint) (num ^ IceCryptoTransform.SXor[0, index2]), (uint) IceCryptoTransform.SMod[0, index2]) << 24);
        IceCryptoTransform.SBox[1, index1] = IceCryptoTransform.Perm32(IceCryptoTransform.GFExp7((uint) (num ^ IceCryptoTransform.SXor[1, index2]), (uint) IceCryptoTransform.SMod[1, index2]) << 16);
        IceCryptoTransform.SBox[2, index1] = IceCryptoTransform.Perm32(IceCryptoTransform.GFExp7((uint) (num ^ IceCryptoTransform.SXor[2, index2]), (uint) IceCryptoTransform.SMod[2, index2]) << 8);
        IceCryptoTransform.SBox[3, index1] = IceCryptoTransform.Perm32(IceCryptoTransform.GFExp7((uint) (num ^ IceCryptoTransform.SXor[3, index2]), (uint) IceCryptoTransform.SMod[3, index2]));
      }
    }

    private void BuildSchedule(ushort[] keyBuilder, int n, int keyRotationOffset)
    {
      for (int index1 = 0; index1 < 8; ++index1)
      {
        int num1 = IceCryptoTransform.KeyRotation[keyRotationOffset + index1];
        int index2 = n + index1;
        this.keySchedule[index2, 0] = 0U;
        this.keySchedule[index2, 1] = 0U;
        this.keySchedule[index2, 2] = 0U;
        for (int index3 = 0; index3 < 15; ++index3)
        {
          for (int index4 = 0; index4 < 4; ++index4)
          {
            ushort num2 = keyBuilder[num1 + index4 & 3];
            ushort num3 = (ushort) ((uint) num2 & 1U);
            this.keySchedule[index2, index3 % 3] = this.keySchedule[index2, index3 % 3] << 1 | (uint) num3;
            keyBuilder[num1 + index4 & 3] = (ushort) ((int) num2 >> 1 | ((int) num3 ^ 1) << 15);
          }
        }
      }
    }

    private void SetKey(byte[] key)
    {
      ushort[] keyBuilder = new ushort[4];
      if (this.rounds == 8)
      {
        if (key.Length != 8)
          throw new ArgumentException("Key size is not valid.", nameof (key));
        for (int index = 0; index < 4; ++index)
          keyBuilder[3 - index] = (ushort) ((uint) key[index << 1] << 8 | (uint) key[(index << 1) + 1]);
        this.BuildSchedule(keyBuilder, 0, 0);
      }
      else
      {
        if (key.Length != this.size << 3)
          throw new ArgumentException("Key size is not valid.", nameof (key));
        for (int index1 = 0; index1 < this.size; ++index1)
        {
          int n = index1 << 3;
          for (int index2 = 0; index2 < 4; ++index2)
            keyBuilder[3 - index2] = (ushort) ((uint) key[n + (index2 << 1)] << 8 | (uint) key[n + (index2 << 1) + 1]);
          this.BuildSchedule(keyBuilder, n, 0);
          this.BuildSchedule(keyBuilder, this.rounds - 8 - n, 8);
        }
      }
    }

    private uint Transform(uint value, int subKeyIndex)
    {
      uint num1 = (uint) ((int) (value >> 16) & 1023 | ((int) (value >> 14) | (int) value << 18) & 1047552);
      uint num2 = (uint) ((int) value & 1023 | (int) value << 2 & 1047552);
      uint num3 = this.keySchedule[subKeyIndex, 2] & (num1 ^ num2);
      uint num4 = num3 ^ num2;
      uint num5 = num3 ^ num1 ^ this.keySchedule[subKeyIndex, 0];
      uint num6 = num4 ^ this.keySchedule[subKeyIndex, 1];
      return IceCryptoTransform.SBox[0, (int) (IntPtr) (num5 >> 10)] | IceCryptoTransform.SBox[1, (int) (IntPtr) (num5 & 1023U)] | IceCryptoTransform.SBox[2, (int) (IntPtr) (num6 >> 10)] | IceCryptoTransform.SBox[3, (int) (IntPtr) (num6 & 1023U)];
    }

    private void Encrypt(byte[] input, int inputOffset, byte[] output, int outputOffset)
    {
      uint num1 = (uint) ((int) input[inputOffset] << 24 | (int) input[inputOffset + 1] << 16 | (int) input[inputOffset + 2] << 8) | (uint) input[inputOffset + 3];
      uint num2 = (uint) ((int) input[inputOffset + 4] << 24 | (int) input[inputOffset + 5] << 16 | (int) input[inputOffset + 6] << 8) | (uint) input[inputOffset + 7];
      for (int subKeyIndex = 0; subKeyIndex < this.rounds; subKeyIndex += 2)
      {
        num1 ^= this.Transform(num2, subKeyIndex);
        num2 ^= this.Transform(num1, subKeyIndex + 1);
      }
      output[outputOffset] = (byte) (num2 >> 24 & (uint) byte.MaxValue);
      output[outputOffset + 1] = (byte) (num2 >> 16 & (uint) byte.MaxValue);
      output[outputOffset + 2] = (byte) (num2 >> 8 & (uint) byte.MaxValue);
      output[outputOffset + 3] = (byte) (num2 & (uint) byte.MaxValue);
      output[outputOffset + 4] = (byte) (num1 >> 24 & (uint) byte.MaxValue);
      output[outputOffset + 5] = (byte) (num1 >> 16 & (uint) byte.MaxValue);
      output[outputOffset + 6] = (byte) (num1 >> 8 & (uint) byte.MaxValue);
      output[outputOffset + 7] = (byte) (num1 & (uint) byte.MaxValue);
    }

    private void Decrypt(byte[] input, int inputOffset, byte[] output, int outputOffset)
    {
      uint num1 = (uint) ((int) input[inputOffset] << 24 | (int) input[inputOffset + 1] << 16 | (int) input[inputOffset + 2] << 8) | (uint) input[inputOffset + 3];
      uint num2 = (uint) ((int) input[inputOffset + 4] << 24 | (int) input[inputOffset + 5] << 16 | (int) input[inputOffset + 6] << 8) | (uint) input[inputOffset + 7];
      for (int subKeyIndex = this.rounds - 1; subKeyIndex > 0; subKeyIndex -= 2)
      {
        num1 ^= this.Transform(num2, subKeyIndex);
        num2 ^= this.Transform(num1, subKeyIndex - 1);
      }
      output[outputOffset] = (byte) (num2 >> 24 & (uint) byte.MaxValue);
      output[outputOffset + 1] = (byte) (num2 >> 16 & (uint) byte.MaxValue);
      output[outputOffset + 2] = (byte) (num2 >> 8 & (uint) byte.MaxValue);
      output[outputOffset + 3] = (byte) (num2 & (uint) byte.MaxValue);
      output[outputOffset + 4] = (byte) (num1 >> 24 & (uint) byte.MaxValue);
      output[outputOffset + 5] = (byte) (num1 >> 16 & (uint) byte.MaxValue);
      output[outputOffset + 6] = (byte) (num1 >> 8 & (uint) byte.MaxValue);
      output[outputOffset + 7] = (byte) (num1 & (uint) byte.MaxValue);
    }

    public bool CanReuseTransform
    {
      get
      {
        return false;
      }
    }

    public bool CanTransformMultipleBlocks
    {
      get
      {
        return true;
      }
    }

    public int InputBlockSize
    {
      get
      {
        return 8;
      }
    }

    public int OutputBlockSize
    {
      get
      {
        return 8;
      }
    }

    public int TransformBlock(
      byte[] inputBuffer,
      int inputOffset,
      int inputCount,
      byte[] outputBuffer,
      int outputOffset)
    {
      if (inputBuffer == null)
        throw new ArgumentNullException(nameof (inputBuffer));
      if (inputOffset < 0)
        throw new ArgumentOutOfRangeException(nameof (inputOffset));
      if (inputOffset + inputCount > inputBuffer.Length)
        throw new ArgumentOutOfRangeException(nameof (inputCount));
      if (outputBuffer == null)
        throw new ArgumentNullException(nameof (outputBuffer));
      if (outputOffset < 0)
        throw new ArgumentOutOfRangeException(nameof (outputOffset));
      if (outputOffset + inputCount > outputBuffer.Length)
        throw new ArgumentOutOfRangeException(nameof (inputCount));
      if (this.encrypt)
      {
        for (int index = 0; index < inputCount; index += 8)
        {
          this.Encrypt(inputBuffer, inputOffset, outputBuffer, outputOffset);
          inputOffset += 8;
          outputOffset += 8;
        }
      }
      else
      {
        for (int index = 0; index < inputCount; index += 8)
        {
          this.Decrypt(inputBuffer, inputOffset, outputBuffer, outputOffset);
          inputOffset += 8;
          outputOffset += 8;
        }
      }
      return inputCount;
    }

    public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
    {
      if (inputBuffer == null)
        throw new ArgumentNullException(nameof (inputBuffer));
      if (inputOffset < 0)
        throw new ArgumentOutOfRangeException(nameof (inputOffset));
      if (inputOffset + inputCount > inputBuffer.Length)
        throw new ArgumentOutOfRangeException(nameof (inputCount));
      byte[] output = new byte[inputCount + 7 & -8];
      int outputOffset = 0;
      if (this.encrypt)
      {
        for (int index = 0; index < inputCount; index += 8)
        {
          this.Encrypt(inputBuffer, inputOffset, output, outputOffset);
          inputOffset += 8;
          outputOffset += 8;
        }
      }
      else
      {
        for (int index = 0; index < inputCount; index += 8)
        {
          this.Decrypt(inputBuffer, inputOffset, output, outputOffset);
          inputOffset += 8;
          outputOffset += 8;
        }
      }
      return output;
    }

    public void Dispose()
    {
      this.size = 0;
      this.rounds = 0;
      for (int index1 = 0; index1 < this.keySchedule.GetLength(0); ++index1)
      {
        for (int index2 = 0; index2 < this.keySchedule.GetLength(1); ++index2)
          this.keySchedule[index1, index2] = 0U;
      }
      this.keySchedule = (uint[,]) null;
    }
  }
}
