﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.NexonArchive
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace Nexon.CSO
{
  public sealed class NexonArchive : IDisposable
  {
    private static readonly byte[] HeaderXor = new byte[16]
    {
      (byte) 25,
      (byte) 91,
      (byte) 123,
      (byte) 44,
      (byte) 101,
      (byte) 94,
      (byte) 121,
      (byte) 37,
      (byte) 110,
      (byte) 75,
      (byte) 7,
      (byte) 33,
      (byte) 98,
      (byte) 127,
      (byte) 0,
      (byte) 41
    };
    private List<NexonArchiveFileEntry> fileEntries = new List<NexonArchiveFileEntry>();
    private Stream stream;

    internal Stream Stream
    {
      get
      {
        return this.stream;
      }
    }

    public ReadOnlyCollection<NexonArchiveFileEntry> FileEntries
    {
      get
      {
        return this.fileEntries.AsReadOnly();
      }
    }

    public void Load(string fileName, bool writable)
    {
      if (this.stream != null)
        throw new InvalidOperationException("The archive must be disposed before it can be loaded again.");
      this.Load((Stream) new FileStream(fileName, FileMode.Open, writable ? FileAccess.ReadWrite : FileAccess.Read, FileShare.Read), writable);
    }

    public void Load(Stream stream, bool writable)
    {
      if (stream == null)
        throw new ArgumentNullException(nameof (stream));
      if (!stream.CanRead)
        throw new ArgumentException("Cannot read from stream.", nameof (stream));
      if (!stream.CanSeek)
        throw new ArgumentException("Cannot seek in stream.", nameof (stream));
      if (writable && !stream.CanWrite)
        throw new ArgumentException("Cannot write to stream.", nameof (stream));
      if (this.stream != null)
        throw new InvalidOperationException("The archive must be disposed before it can be loaded again.");
      int num;
      byte[] buffer;
      lock (stream)
      {
        stream.Position = 0L;
        this.stream = stream;
        BinaryReader binaryReader = new BinaryReader(this.stream);
        if (binaryReader.ReadInt32() != 5390670)
          throw new InvalidDataException("NAR file signature is invalid.");
        if (binaryReader.ReadInt32() != 16777216)
          throw new InvalidDataException("NAR file version is invalid.");
        if (this.stream.Length < 16L)
          throw new InvalidDataException("NAR file is not long enough to be valid.");
        this.stream.Seek(-4L, SeekOrigin.End);
        if (binaryReader.ReadInt32() != 5390670)
          throw new InvalidDataException("NAR end file signature is invalid.");
        this.stream.Seek(-8L, SeekOrigin.Current);
        num = binaryReader.ReadInt32() ^ 1081496863;
        if (this.stream.Length < (long) (num + 16))
          throw new InvalidDataException("NAR file is not long enough to be valid.");
        this.stream.Seek((long) (-4 - num), SeekOrigin.Current);
        buffer = binaryReader.ReadBytes(num);
      }
      for (int index = 0; index < buffer.Length; ++index)
        buffer[index] ^= NexonArchive.HeaderXor[index & 15];
      using (MemoryStream memoryStream = new MemoryStream(num))
      {
        ICSharpCode.SharpZipLib.BZip2.BZip2.Decompress((Stream) new MemoryStream(buffer, false), (Stream) memoryStream);
        this.LoadHeader(memoryStream.ToArray());
      }
    }

    private void LoadHeader(byte[] header)
    {
      if (header.Length < 4)
        throw new InvalidDataException("NAR header is invalid.");
      if (BitConverter.ToInt32(header, 0) != 1)
        throw new InvalidDataException("NAR header version is invalid.");
      if (header.Length < 16)
        throw new InvalidDataException("NAR header is invalid.");
      BitConverter.ToInt32(header, 4);
      BitConverter.ToInt32(header, 8);
      BitConverter.ToInt32(header, 12);
      int int32 = BitConverter.ToInt32(header, 16);
      if (int32 < 0)
        throw new InvalidDataException("Directory entry count is too large.");
      int offset = 20;
      for (int index = 0; index < int32; ++index)
      {
        NexonArchiveFileEntry archiveFileEntry = new NexonArchiveFileEntry(this);
        offset += archiveFileEntry.Load(header, offset);
        this.fileEntries.Add(archiveFileEntry);
      }
    }

    public void Close()
    {
      this.fileEntries.Clear();
      this.stream.Close();
      this.stream = (Stream) null;
    }

    public void Dispose()
    {
      this.Close();
    }
  }
}
