﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.CircularBuffer
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;

namespace Nexon.CSO
{
  internal sealed class CircularBuffer
  {
    private byte[] data;
    private int source;
    private int length;

    public CircularBuffer(int length)
    {
      this.data = new byte[length];
    }

    public int Length
    {
      get
      {
        return this.length;
      }
    }

    public void Append(byte[] buffer, int offset, int count)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (offset < 0)
        throw new ArgumentOutOfRangeException(nameof (offset));
      if (count < 0 || offset + count > buffer.Length)
        throw new ArgumentOutOfRangeException(nameof (count));
      if (count == 0)
        return;
      if (count >= this.data.Length)
      {
        Buffer.BlockCopy((Array) buffer, offset + (count - this.data.Length), (Array) this.data, 0, this.data.Length);
        this.source = 0;
        this.length = this.data.Length;
      }
      else
      {
        if (this.source == this.data.Length)
          this.source = 0;
        int count1 = Math.Min(this.data.Length - this.source, count);
        Buffer.BlockCopy((Array) buffer, offset, (Array) this.data, this.source, count1);
        if (count > count1)
          Buffer.BlockCopy((Array) buffer, offset + count1, (Array) this.data, 0, count - count1);
        this.source = (this.source + count) % this.data.Length;
        this.length = Math.Min(this.length + count, this.data.Length);
      }
    }

    public void Copy(int distance, byte[] buffer, int offset, int count)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (offset < 0)
        throw new ArgumentOutOfRangeException(nameof (offset));
      if (count < 0 || offset + count > buffer.Length || count > this.length)
        throw new ArgumentOutOfRangeException(nameof (count));
      if (distance <= 0 || distance > this.length)
        throw new ArgumentOutOfRangeException(nameof (distance));
      if (count == 0)
        return;
      int srcOffset = this.source - distance;
      if (srcOffset < 0)
        srcOffset = this.data.Length + srcOffset;
      int val2 = this.data.Length - srcOffset;
      int count1 = Math.Min(count, val2);
      Buffer.BlockCopy((Array) this.data, srcOffset, (Array) buffer, offset, count1);
      if (count <= val2)
        return;
      Buffer.BlockCopy((Array) this.data, 0, (Array) buffer, offset + count1, count - count1);
    }
  }
}
