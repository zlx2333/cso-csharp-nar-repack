﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.BoundedStream
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.IO;

namespace Nexon.CSO
{
  internal sealed class BoundedStream : Stream
  {
    private Stream baseStream;
    private long baseOffset;
    private long baseLength;
    private long position;

    public BoundedStream(Stream stream, long offset, long length)
    {
      this.baseStream = stream;
      this.baseOffset = offset;
      this.baseLength = length;
      this.position = this.baseStream.Position - this.baseOffset;
      if (this.position >= 0L)
        return;
      this.baseStream.Seek(-this.position, SeekOrigin.Current);
      this.position = 0L;
    }

    public override bool CanRead
    {
      get
      {
        return this.baseStream.CanRead;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return this.baseStream.CanSeek;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return this.baseStream.CanWrite;
      }
    }

    public override long Length
    {
      get
      {
        return this.baseLength;
      }
    }

    public override long Position
    {
      get
      {
        return this.position;
      }
      set
      {
        if (!this.CanSeek)
          throw new NotSupportedException("Cannot seek in stream.");
        if (value < 0L || value > this.baseLength)
          throw new ArgumentOutOfRangeException(nameof (value));
        this.baseStream.Position = this.baseOffset + value;
        this.position = this.baseStream.Position - this.baseOffset;
      }
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      if (!disposing)
        return;
      this.baseStream.Dispose();
    }

    public override void Flush()
    {
      this.baseStream.Flush();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (offset < 0)
        throw new ArgumentOutOfRangeException(nameof (offset));
      if (count < 0 || offset + count > buffer.Length)
        throw new ArgumentOutOfRangeException(nameof (count));
      if (this.position + (long) count > this.baseLength)
        count = Convert.ToInt32(Math.Min(this.baseLength - this.position, (long) int.MaxValue));
      int num = this.baseStream.Read(buffer, offset, count);
      this.position += (long) num;
      return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      if (!this.CanSeek)
        throw new NotSupportedException("Cannot seek in stream.");
      long num1;
      switch (origin)
      {
        case SeekOrigin.Begin:
          if (offset < 0L || offset > this.baseLength)
            throw new ArgumentOutOfRangeException(nameof (offset));
          num1 = this.baseStream.Seek(this.baseOffset + offset, SeekOrigin.Begin);
          break;
        case SeekOrigin.Current:
          long num2 = this.position + offset;
          if (num2 < 0L || num2 > this.baseLength)
            throw new ArgumentOutOfRangeException(nameof (offset));
          num1 = this.baseStream.Seek(offset, SeekOrigin.Current);
          break;
        case SeekOrigin.End:
          long num3 = this.baseLength + offset;
          if (num3 < 0L || num3 > this.baseLength)
            throw new ArgumentOutOfRangeException(nameof (offset));
          num1 = this.baseStream.Seek(offset, SeekOrigin.End);
          break;
        default:
          throw new ArgumentException("Not a valid seek origin.", nameof (origin));
      }
      this.position = num1 - this.baseOffset;
      return this.position;
    }

    public override void SetLength(long value)
    {
      if (value < this.baseOffset + this.baseLength)
        throw new ArgumentException("Value is less than the stream's boundaries.", nameof (value));
      this.baseStream.SetLength(value);
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (offset < 0)
        throw new ArgumentOutOfRangeException(nameof (offset));
      if (count < 0 || offset + count > buffer.Length)
        throw new ArgumentOutOfRangeException(nameof (count));
      if (this.position + (long) count > this.baseLength)
        throw new ArgumentOutOfRangeException(nameof (count));
      this.baseStream.Write(buffer, offset, count);
      this.position += (long) count;
    }
  }
}
