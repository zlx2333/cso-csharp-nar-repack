﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.NexonArchiveFileDecompressStream
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.IO;

namespace Nexon.CSO
{
  internal sealed class NexonArchiveFileDecompressStream : Stream
  {
    private CircularBuffer dictionary = new CircularBuffer(8192);
    private Stream baseStream;
    private long outputLength;
    private long outputPosition;
    private int lastReadDistance;
    private int lastReadLength;

    public NexonArchiveFileDecompressStream(Stream stream, long length)
    {
      this.baseStream = stream;
      this.outputLength = length;
    }

    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return false;
      }
    }

    public override long Length
    {
      get
      {
        return this.outputLength;
      }
    }

    public override long Position
    {
      get
      {
        return this.outputPosition;
      }
      set
      {
        throw new NotSupportedException("Cannot seek in stream.");
      }
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      if (!disposing)
        return;
      this.baseStream.Dispose();
    }

    public override void Flush()
    {
    }

    private byte ReadByteChecked()
    {
      int num = this.baseStream.ReadByte();
      if (num < 0)
        throw new EndOfStreamException();
      return Convert.ToByte(num);
    }

    private void ReadHeader()
    {
      byte num1 = this.ReadByteChecked();
      int num2 = (int) num1 >> 5;
      int num3 = (int) num1 & 31;
      switch (num2)
      {
        case 0:
          this.lastReadDistance = 0;
          this.lastReadLength = num3 + 1;
          return;
        case 7:
          num2 += (int) this.ReadByteChecked();
          break;
      }
      int num4 = num2 + 2;
      this.lastReadDistance = (num3 << 8 | (int) this.ReadByteChecked()) + 1;
      this.lastReadLength = num4;
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (offset < 0)
        throw new ArgumentOutOfRangeException(nameof (offset));
      if (count < 0 || offset + count > buffer.Length)
        throw new ArgumentOutOfRangeException(nameof (count));
      if (count == 0 || this.baseStream.Position >= this.baseStream.Length || this.Position >= this.Length)
        return 0;
      count = Convert.ToInt32(Math.Min(this.Length - this.Position, (long) count));
      int num1 = count;
      int num2 = num1;
      while (num1 > 0)
      {
        if (this.lastReadLength == 0)
        {
          this.ReadHeader();
          if (this.lastReadDistance > 0 && this.lastReadDistance > this.dictionary.Length)
            throw new InvalidDataException("Distance is larger than the dictionary's current length.");
        }
        if (count > this.lastReadLength)
          count = this.lastReadLength;
        if (this.lastReadDistance == 0)
        {
          int count1 = this.baseStream.Read(buffer, offset, count);
          if (count1 == 0)
            throw new EndOfStreamException("Expected " + (object) this.lastReadLength + " more bytes in compressed stream.");
          this.dictionary.Append(buffer, offset, count1);
          this.lastReadLength -= count1;
          this.outputPosition += (long) count1;
          num1 -= count1;
          offset += count1;
        }
        else
        {
          for (; count > 0; --count)
          {
            this.dictionary.Copy(this.lastReadDistance, buffer, offset, 1);
            this.dictionary.Append(buffer, offset, 1);
            --this.lastReadLength;
            ++this.outputPosition;
            --num1;
            ++offset;
          }
        }
        count = num1;
      }
      return num2;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotSupportedException("Cannot seek in stream.");
    }

    public override void SetLength(long value)
    {
      throw new NotSupportedException("Cannot write to stream.");
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotSupportedException("Cannot write to stream.");
    }
  }
}
