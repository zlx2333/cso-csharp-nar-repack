﻿// Decompiled with JetBrains decompiler
// Type: Nexon.CSO.NexonArchiveFileDecoderStream
// Assembly: CSOToolLib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18BD017C-A01A-46BA-A603-DC56387BEB0B
// Assembly location: D:\PortablePrograms\csotools-2,0\CSOToolLib.dll

using System;
using System.IO;
using System.Text;

namespace Nexon.CSO
{
  internal sealed class NexonArchiveFileDecoderStream : Stream
  {
    private byte[] key = new byte[16];
    private Stream baseStream;

    public NexonArchiveFileDecoderStream(Stream stream, string path)
    {
      this.baseStream = stream;
      this.GenerateKey(path);
    }

    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return true;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return false;
      }
    }

    public override long Length
    {
      get
      {
        return this.baseStream.Length;
      }
    }

    public override long Position
    {
      get
      {
        return this.baseStream.Position;
      }
      set
      {
        this.baseStream.Position = value;
      }
    }

    private static uint PythonHash(byte[] data)
    {
      uint num = 0;
      for (int index = 0; index < data.Length; ++index)
        num = num * 1000003U ^ (uint) data[index];
      return num ^ (uint) data.Length;
    }

    private void GenerateKey(string path)
    {
      uint num = NexonArchiveFileDecoderStream.PythonHash(Encoding.GetEncoding("euc-kr").GetBytes(path));
      for (int index = 0; index < 16; ++index)
      {
        num = (uint) ((int) num * 1103515245 + 12345);
        this.key[index] = (byte) (num & (uint) byte.MaxValue);
      }
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      if (!disposing)
        return;
      this.baseStream.Dispose();
    }

    public override void Flush()
    {
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      long position = this.Position;
      int num = this.baseStream.Read(buffer, offset, count);
      for (int index = 0; index < num; ++index)
        buffer[offset + index] ^= this.key[position + (long) index & 15L];
      return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      return this.baseStream.Seek(offset, origin);
    }

    public override void SetLength(long value)
    {
      throw new NotSupportedException("Cannot write to stream.");
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotSupportedException("Cannot write to stream.");
    }
  }
}
